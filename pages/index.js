import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { Navbar } from 'react-bootstrap-v5';
import { Nav } from 'react-bootstrap-v5';
import "bootstrap/dist/css/bootstrap.css";

export default function Home() {
  return (
    <div>
<Head>
        <title>دلتا توس</title>
        <link rel="icon" href="/logo.jpg" />
      </Head>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Navbar.Brand href="#home">شرکت فنی و مهندسی دلتا توس</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="#aboutus">درباره ما</Nav.Link>
      <Nav.Link href="#events">رویداد ها</Nav.Link>
      <Nav.Link href="#contactus">تماس با ما</Nav.Link>
      <Nav.Link href="#signup">عضویت</Nav.Link>
      <Nav.Link href="#contactus">ورود اعضا</Nav.Link>
    </Nav>
    <Nav>
      <Nav.Link href="#deets">بیشتر...</Nav.Link>
      <Nav.Link eventKey={2} href="#memes">
        لینک های مفید
      </Nav.Link>
    </Nav>
  </Navbar.Collapse>
</Navbar>
    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">deltatoos</a>
        </h1>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://alireza-hosseini.web.app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by Alireza Hosseini
        </a>
      </footer>
    </div>
    </div>
      
  )
}
